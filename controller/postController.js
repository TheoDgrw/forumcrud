const con = require('../util/database');
const moment = require('moment');

exports.postAddPost = (req, res, next) => {
    con.query('INSERT INTO post SET content = ?, author = ?, topic_id = ?, date = ?'
        , [req.body.content, req.body.author, req.body.topicId, moment().format()]
        , (err, results, fields) => {
            if (err) throw err;
            res.json(results);
        }
    );
}

exports.getPost = (req, res, next) => {
    con.query('SELECT * FROM post p WHERE p.id = ?', [req.body.id], (err, results, fields) => {
        if (err) throw err;
        res.json(results);
    });
}
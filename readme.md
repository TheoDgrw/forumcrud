The API isn't complete yet.

## installation

- npm install

## create database

you need to create a mysql database and call it "forum".
Then check the database file inside the utils folder.
Edit the user and password values with your credentials.

## launch

- npm start-server

## request

### add a post

url: http://localhost:3000/add-post
header: "Content-type": "application/json"
body: {
    "content": "some text",
    "author": "a name,
    "topicId": "a topic column number"
}

### add a post

url: http://localhost:3000/add-topic
header: "Content-type": "application/json"
body: {
    "title": "sometitle"
}
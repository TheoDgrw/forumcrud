const path = require('path');
const express = require('express');

const topicController = require('../controller/topicController');

const router = express.Router();

router.post('/add-topic', topicController.postAddTopic);

module.exports = router;
const path = require('path');
const express = require('express');

const postController = require('../controller/postController');

const router = express.Router();

router.post('/add-post', postController.postAddPost);

module.exports = router;
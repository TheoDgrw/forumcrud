const bodyParser = require('body-parser');
const express = require('express');

const con = require('./util/database');

const topicRoutes = require('./routes/topic');
const postRoutes = require('./routes/post');

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(topicRoutes);
app.use(postRoutes);

con.connect(err => {
    if (err) throw err;
    con.query("CREATE TABLE IF NOT EXISTS topic (id INT AUTO_INCREMENT PRIMARY KEY, title TEXT)");
    con.query("CREATE TABLE IF NOT EXISTS post (id INT AUTO_INCREMENT PRIMARY KEY, content TEXT, author TEXT, date DATETIME, topic_id INT)");
    con.query('ALTER TABLE post ADD FOREIGN KEY (topic_id) REFERENCES topic(id)');
});

app.listen(3000);